'use strict';

// forEach IE11
if (window.NodeList && !NodeList.prototype.forEach) {
  NodeList.prototype.forEach = Array.prototype.forEach;
}

(function(ELEMENT) {
  ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
  ELEMENT.closest = ELEMENT.closest || function closest(selector) {
      if (!this) return null;
      if (this.matches(selector)) return this;
      if (!this.parentElement) {return null}
      else return this.parentElement.closest(selector)
    };
}(Element.prototype));


//main
var TIMEOUT_IN_MS = 10000;

var URL_GET = 'https://jsonplaceholder.typicode.com/posts/1';

var StatusCode = {
  OK: 200
};

var cart = {};

var btnCart = document.querySelectorAll('.item__btn-cart');
var btnBuy = document.querySelectorAll('.item__btn-buy');
var btn = document.querySelectorAll('.item__btn');
var item = document.querySelectorAll('.gallery__item');

var toggClass = function (el) {
  var parent = el.closest('.gallery__item');
  if (parent.classList.contains('item--incart')) {
    parent.classList.remove('item--incart');
  } else {
    parent.classList.add('item--incart');
  }
};

var addToCard = function (el) {
  var articul = el.getAttribute('data-art');
  if (cart[articul] !== undefined && cart !== null) {
    delete cart[articul];
    toggClass(el);
  }
  else {
    cart[articul] = 1;
    toggClass(el);
  }

  localStorage.setItem('cart', JSON.stringify(cart));
};

btn.forEach(function (element) {
  element.addEventListener('click', function () {
    addToCard(element);
    sendRequest('GET', URL_GET);
  });
});

var checkCart = function () {
  cart = JSON.parse(localStorage.getItem('cart'));
  if (cart !== undefined && cart !== null) {
    Object.keys(cart).forEach(function(key) {
      btnBuy.forEach(function (element) {
        var art = element.getAttribute('data-art');
        if (art == key) {
          var par = element.closest('.gallery__item');
          par.classList.add('item--incart');
        }
      });
    }, cart);
  }
};
checkCart();

var sendRequest = function (method, url, body) {
  return fetch(url)
  .then(function (response) {
    if (!response.ok) {
      return response.json().then(function (err) {
        throw new Error ('Статус ответа: ' + response.status + ' ' + response.statusText);
      })
    }
    return response.json();
  })
};
